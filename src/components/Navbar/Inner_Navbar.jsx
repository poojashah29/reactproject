import React, { Component } from "react";
import "../../style/App.css";
import Title from "../HomePage/Title";
import { Nav, NavItem, Navbar } from "react-bootstrap";
import facebook from "../../icons/facebook.png";
import google from "../../icons/google.png";
import linkdin from "../../icons/linkdin.png";
import twitter from "../../icons/twitter.png";
import pinterest from "../../icons/pinterest.png";

class Inner_Navbar extends Component {
  render() {
    return (
      <div style={{ background: "#4193ea" }}>
        <Navbar className="navInverse" id="inner-navbar">
          <Title />
          <div className="logout">
            <a style={{ textDecoration: "none" }} href="/">
              <h5>MY ACCOUNT</h5>
            </a>
            <a style={{ textDecoration: "none" }} href="/">
              <h5>LOGOUT</h5>
            </a>
          </div>
          <Navbar.Header>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse style={{ background: "#0358a0" }}>
            <Nav>
              <NavItem eventKey={1} href="#">
                <span className="dropdownColor"> FEATURES </span>
              </NavItem>
              <NavItem eventKey={2} href="#">
                <span className="dropdownColor"> MEMBERSHIP </span>
              </NavItem>
              <NavItem eventKey={3} href="#">
                <span className="dropdownColor"> COMMUNITY </span>
              </NavItem>
              <NavItem eventKey={4} href="#">
                <span className="dropdownColor"> NEWS </span>
              </NavItem>
              <NavItem eventKey={5} href="#">
                <span className="dropdownColor"> EVENTS </span>
              </NavItem>
              <NavItem eventKey={6} href="#">
                <span className="dropdownColor"> CONTACT US </span>
              </NavItem>
            </Nav>
            <Nav pullRight style={{ paddingTop: "17px" }}>
              <a href="https://www.facebook.com/" target="_blank">
                <img className="navbar-Two-Icon" src={facebook} alt="" />
              </a>
              <a href="https://twitter.com/" target="_blank">
                <img className="navbar-Two-Icon" src={twitter} alt="" />
              </a>
              <a href="https://www.google.com/" target="_blank">
                <img className="navbar-Two-Icon" src={google} alt="" />
              </a>
              <a href="https://www.pinterest.com/" target="_blank">
                <img className="navbar-Two-Icon" src={pinterest} alt="" />
              </a>
              <a href="https://www.linkedin.com/" target="_blank">
                <img className="navbar-Two-Icon" src={linkdin} alt="" />
              </a>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

export default Inner_Navbar;
