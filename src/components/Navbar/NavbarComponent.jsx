import React, { Component } from "react";
import "../../style/App.css";
import Title from "../HomePage/Title";
import { Nav, NavItem, Navbar } from "react-bootstrap";

class NavbarComponent extends Component {
  render() {
    return (
      <Navbar inverse className="navInverse" collapseOnSelect>
        <Title />
        <Navbar.Header>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <NavItem className="btn_border" eventKey={1} href="#">
              <span className="dropdownColor"> FEATURES </span>{" "}
            </NavItem>
            <NavItem className="btn_border" eventKey={2} href="#">
              <span className="dropdownColor"> MEMBERSHIP </span>
            </NavItem>
            <NavItem className="btn_border" eventKey={3} href="#">
              <span className="dropdownColor"> COMMUNITY </span>
            </NavItem>
            <NavItem className="btn_border" eventKey={4} href="#">
              <span className="dropdownColor"> NEWS </span>
            </NavItem>
            <NavItem className="btn_border" eventKey={5} href="#">
              <span className="dropdownColor"> EVENTS </span>
            </NavItem>
            <NavItem eventKey={6} href="#">
              <span className="dropdownColor"> CONTACT US </span>
            </NavItem>
          </Nav>
          <Nav pullRight>
            <NavItem eventKey={7} href="#/signin" id="logbutton">
              <button type="button" className="btnAlign">
                LOGIN / REGISTER
              </button>
            </NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default NavbarComponent;
