import React, { Component } from "react";
import { Button, Checkbox, Row, Col, Input } from "antd";
import Share from "../HomePage/share.jsx";
import Footer from "../Footer/Footer.jsx";
import NavbarComponent from "../Navbar/NavbarComponent.jsx";
import { Link } from "react-router";

class SignIn extends Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {}
    };
    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(
      this
    );
  }
  redirectPage() {
    window.location.hash = "/MyProfile";
  }
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  submituserRegistrationForm(e) {
    e.preventDefault();
    if (this.validateForm()) {
      this.redirectPage();
      let fields = {};
      fields["emailid"] = "";
      fields["password"] = "";
      this.setState({ fields: fields });
    }
  }

  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["emailid"]) {
      formIsValid = false;
      errors["emailid"] = "Please enter your email";
    }

    if (typeof fields["emailid"] !== "undefined") {
      //regular expression for email validation
      var pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(fields["emailid"])) {
        formIsValid = false;
        errors["emailid"] = "Please enter a valid email";
      }
    }

    //-------------------------------------//

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "Please enter your password";
    } else if (fields["password"].length <= 5) {
      formIsValid = false;
      errors["password"] = "password should be atleast 6 characters";
    }

    //Must be at least 8 characters
    // At least 1 special character from @#$ %&
    // At least 1 number, 1 lowercase, 1 uppercase letter

    //limitatins

    // if (typeof fields["password"] !== "undefined") {
    //   if (
    //     !fields["password"].match(
    //       "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
    //     )
    //   ) {
    //     formIsValid = false;
    //     errors["password"] = "*Please enter secure and strong password.";
    //   }
    // }

    // if (!document.getElementById("checkbox").checked) {
    //   formIsValid = false;
    //   errors["checkbox"] = "*Please enter secure and strong password.";
    // }

    this.setState({ errors: errors });

    return formIsValid;
  }

  render() {
    return (
      <div id="signIn">
        <div className="LoginBackground">
          <NavbarComponent />
          <div className="modal-dialog">
            <div className="modal-content setPos_Model">
              <div className="modal-body">
                <form>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <h2>Log&nbsp;in</h2>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <span className="inputfield-heading-color">Email</span>
                      <Input
                        placeholder="Enter Email"
                        type="email"
                        id="emailid"
                        name="emailid"
                        size="large"
                        autoComplete="off"
                        className="inputBackground"
                        value={this.state.fields.emailid}
                        onChange={this.handleChange}
                      />
                      <div className="errorMsg">
                        {this.state.errors.emailid}
                      </div>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <span className="inputfield-heading-color">Password</span>
                      <Input
                        placeholder="Enter Password"
                        type="password"
                        id="password"
                        name="password"
                        size="large"
                        autoComplete="off"
                        className="inputBackground"
                        value={this.state.fields.password}
                        onChange={this.handleChange}
                      />
                      <div className="errorMsg">
                        {this.state.errors.password}
                      </div>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <Button
                        type="primary"
                        block
                        size="large"
                        onClick={this.submituserRegistrationForm}
                      >
                        LOGIN
                      </Button>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={9} offset={3}>
                      <Checkbox className="remember">Remember Me</Checkbox>
                    </Col>
                    <Col span={9}>
                      <Link to="/ResetPassword" className="forget_psw">
                        Reset Password
                      </Link>
                    </Col>
                  </Row>
                  <Share />
                  <Row className="member">
                    Not a member ?
                    <Link to="/signup" className="btn btn-link linkclass">
                      Register Now
                    </Link>
                  </Row>
                </form>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default SignIn;
