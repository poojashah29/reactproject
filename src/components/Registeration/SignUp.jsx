import React, { Component } from "react";
import Share from "../HomePage/share.jsx";
import Footer from "../Footer/Footer.jsx";
import NavbarComponent from "../Navbar/NavbarComponent.jsx";
import { Button, Checkbox, Row, Col, Input, Icon } from "antd";
import { Link } from "react-router";

class SignUp extends Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {}
    };
    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(
      this
    );
  }
  redirectPage() {
    window.location.hash = "/CompanyProfile";
  }
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  submituserRegistrationForm(e) {
    e.preventDefault();
    if (this.validateForm()) {
      this.redirectPage();
      let fields = {};
      fields["firstName"] = "";
      fields["lastName"] = "";
      fields["emailid"] = "";
      fields["password"] = "";
      this.setState({ fields: fields });
    }
  }

  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["firstName"]) {
      formIsValid = false;
      errors["firstName"] = "Please enter your first name.";
    }
    if (!fields["lastName"]) {
      formIsValid = false;
      errors["lastName"] = "Please enter your last  name.";
    }

    //----------------email---------------------//

    if (!fields["emailid"]) {
      formIsValid = false;
      errors["emailid"] = "Please enter your email";
    }

    if (typeof fields["emailid"] !== "undefined") {
      //regular expression for email validation
      var pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(fields["emailid"])) {
        formIsValid = false;
        errors["emailid"] = "Please enter a valid email";
      }
    }

    //----------------password---------------------//

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "Please enter your password";
    } else if (fields["password"].length <= 5) {
      formIsValid = false;
      errors["password"] = "password should be atleast 6 characters";
    }

    if (!fields["Confpassword"]) {
      formIsValid = false;
      errors["Confpassword"] = "Please enter your password";
    } else if (fields["Confpassword"] != fields["password"]) {
      formIsValid = false;
      errors["Confpassword"] = "password does not match";
    }

    //Must be at least 8 characters
    // At least 1 special character from @#$ %&
    // At least 1 number, 1 lowercase, 1 uppercase letter

    // if (typeof fields["password"] !== "undefined") {
    //   if (
    //     !fields["password"].match(
    //       "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
    //     )
    //   ) {
    //     formIsValid = false;
    //     errors["password"] = "*Please enter secure and strong password.";
    //   }
    // }
    // if (!fields["firstName"]) {
    //   formIsValid = false;
    //   errors["firstName"] = "*Please enter First Name";
    // }
    // if (!fields["lastName"]) {
    //   formIsValid = false;
    //   errors["lastName"] = "*Please enter Last Name";
    // }

    // if (!document.getElementById("checkbox").checked) {
    //   formIsValid = false;
    //   errors["checkbox"] = "*Please enter secure and strong password.";
    // }

    //............checkbox...................//

    if (!document.getElementById("checkbox").checked) {
      formIsValid = false;
      alert("You have not agreed to terms of service, please check the box");
    }
    this.setState({ errors: errors });

    return formIsValid;
  }
  showPassword() {
    var x = document.getElementById("pass");

    if (x.type === "password") {
      x.type = "text";
      // iCon.type = "star";
    } else {
      x.type = "password";
    }
  }
  render() {
    return (
      <div id="signUp">
        <div className="LoginBackground">
          <NavbarComponent />
          <div className="modal-dialog">
            <div className="modal-content setPos_Model">
              <div className="modal-body">
                <form>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <h2>Create&nbsp;Account</h2>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <span className="inputfield-heading-color">
                        First Name
                      </span>
                      <Input
                        placeholder="Enter First Name"
                        type="text"
                        name="firstName"
                        autoComplete="off"
                        size="large"
                        className="inputBackground"
                        value={this.state.fields.firstName}
                        onChange={this.handleChange}
                      />
                      <div className="errorMsg">
                        {this.state.errors.firstName}
                      </div>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <span className="inputfield-heading-color">
                        Last Name
                      </span>
                      <Input
                        placeholder="Enter Last Name"
                        type="text"
                        name="lastName"
                        size="large"
                        autoComplete="off"
                        className="inputBackground"
                        value={this.state.fields.lastName}
                        onChange={this.handleChange}
                      />
                      <div className="errorMsg">
                        {this.state.errors.lastName}
                      </div>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <span className="inputfield-heading-color">Email</span>
                      <Input
                        placeholder="Enter Email"
                        type="email"
                        name="emailid"
                        autoComplete="off"
                        size="large"
                        className="inputBackground"
                        value={this.state.fields.emailid}
                        onChange={this.handleChange}
                      />
                      <div className="errorMsg">
                        {this.state.errors.emailid}
                      </div>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <span className="inputfield-heading-color">Password</span>
                      <Input
                        placeholder="Enter Password"
                        type="password"
                        name="password"
                        id="pass"
                        size="large"
                        autoComplete="off"
                        className="inputBackground"
                        value={this.state.fields.password}
                        onChange={this.handleChange}
                        addonAfter={
                          <Icon
                            id="iconP"
                            type="eye"
                            onClick={this.showPassword.bind(this)}
                          />
                        }
                      />
                      <div className="errorMsg">
                        {this.state.errors.password}
                      </div>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <span className="inputfield-heading-color">
                        Confirm Password
                      </span>
                      <Input
                        placeholder="Confirm Password"
                        type="password"
                        name="Confpassword"
                        id="confirmpass"
                        size="large"
                        autoComplete="off"
                        className="inputBackground"
                        value={this.state.fields.Confpassword}
                        onChange={this.handleChange}
                      />
                      <div className="errorMsg">
                        {this.state.errors.Confpassword}
                      </div>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <Button
                        type="primary"
                        block
                        size="large"
                        onClick={this.submituserRegistrationForm}
                      >
                        CREATE AN ACCOUNT
                      </Button>
                    </Col>
                  </Row>
                  <Share />
                  <div className="member">
                    Already a member ?
                    <Link to="/signin" className="btn btn-link linkclass">
                      Login Now
                    </Link>
                  </div>
                  <Row id="remeberRow">
                    <Col span={1} offset={3}>
                      <Checkbox id="checkbox" />
                    </Col>
                    <Col span={18}>
                      <span className="remember">
                        By signing up you indicate that you have read and <br />
                        agree to the &nbsp;
                        <a href="#">Terms of Services</a> and &nbsp;
                        <a href="#">Privacy Policy.</a>
                      </span>
                    </Col>
                  </Row>
                </form>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default SignUp;
