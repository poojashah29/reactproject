import React, { Component } from "react";
import { Row, Col } from "antd";
import doc from "../../images/companyprofile/doc.png";

class Document extends Component {
  constructor(props) {
    super(props);

    this.state = {
      companyTitle: "Executive Summary",
      companyDescription: "Updated 23 June",
      percentage: 67
    };
  }
  render() {
    return (
      <a href="#/MyCompanyProfile" target="blank" style={{ color: "black" }}>
        <div className="col-md-3 col-sm-4 col-xs-12">
          <div className="modal-dialog" id="my_doc">
            <div className="modal-content">
              <div className="modal-body">
                <form style={{ textAlign: "left" }}>
                  <Row>
                    <Col span={3}>
                      <img src={doc} alt="" />
                    </Col>
                    <Col span={20} offset={1}>
                      <h2 className="Doctitle">{this.state.companyTitle}</h2>
                      <p>{this.state.companyDescription}</p>
                    </Col>
                  </Row>
                </form>
              </div>
            </div>
          </div>
        </div>
      </a>
    );
  }
}

export default Document;
