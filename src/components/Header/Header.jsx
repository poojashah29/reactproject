import React, { Component } from "react";
import "../../style/App.css";
import NavbarComponent from "../Navbar/NavbarComponent";
import GetStarted from "../HomePage/GetStarted.jsx";

class Header extends Component {
  render() {
    return (
      <div id="Header" className="backgroundImg">
        <div className="backgroundImg">
          <NavbarComponent />
          <GetStarted />
        </div>
      </div>
    );
  }
}

export default Header;
