import React, { Component } from "react";
import { Row, Col, Input } from "antd";
import Footer from "../Footer/Footer.jsx";
import NavbarComponent from "../Navbar/NavbarComponent.jsx";

class GetNewPassword extends Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {}
    };
    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(
      this
    );
  }
  redirectPage() {
    window.location.hash = "/";
  }
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  submituserRegistrationForm(e) {
    e.preventDefault();
    if (this.validateForm()) {
      this.redirectPage();
      let fields = {};
      fields["Cpassword"] = "";
      fields["Npassword"] = "";
      this.setState({ fields: fields });
    }
  }

  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["Npassword"]) {
      formIsValid = false;
      errors["Npassword"] = "Please enter your password";
    } else if (fields["Npassword"].length <= 5) {
      formIsValid = false;
      errors["Npassword"] = "password should be atleast 6 characters";
    }

    // if (typeof fields["Npassword"] !== "undefined") {
    //   if (
    //     !fields["Npassword"].match(
    //       "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
    //     )
    //   ) {
    //     formIsValid = false;
    //     errors["Npassword"] = "*Please enter secure and strong password.";
    //   }
    // }

    ////////////////////////////////////////////////////////////////////////////////

    if (!fields["Cpassword"]) {
      formIsValid = false;
      errors["Cpassword"] = "Please enter your password";
    } else if (fields["Cpassword"].length <= 5) {
      formIsValid = false;
      errors["Cpassword"] = "password should be atleast 6 characters";
    }

    // if (typeof fields["Cpassword"] !== "undefined") {
    //   if (
    //     !fields["Cpassword"].match(
    //       "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
    //     )
    //   ) {
    //     formIsValid = false;
    //     errors["Cpassword"] = "*Please enter secure and strong password.";
    //   }
    // }

    this.setState({ errors: errors });

    return formIsValid;
  }

  render() {
    return (
      <div id="GetNewPassword">
        <div className="LoginBackground">
          <NavbarComponent />
          <div className="modal-dialog">
            <div className="modal-content setPos_Model">
              <div className="modal-body">
                <form>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <h4>Enter your new password below</h4>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <span className="inputfield-heading-color">Password</span>
                      <Input
                        placeholder="Enter New Password"
                        type="password"
                        name="Npassword"
                        size="large"
                        className="inputBackground"
                        value={this.state.fields.Npassword}
                        onChange={this.handleChange}
                      />
                      <div className="errorMsg">
                        {this.state.errors.Npassword}
                      </div>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <span className="inputfield-heading-color">
                        Confirm Password
                      </span>
                      <Input
                        placeholder="Confirm Password"
                        type="password"
                        name="Cpassword"
                        className="inputBackground"
                        size="large"
                        value={this.state.fields.Cpassword}
                        onChange={this.handleChange}
                      />
                      <div className="errorMsg">
                        {this.state.errors.Cpassword}
                      </div>
                    </Col>
                  </Row>

                  <div id="contactBtn">
                    <button
                      type="submit"
                      id="btn_reset"
                      onClick={this.submituserRegistrationForm}
                    >
                      Reset Password
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default GetNewPassword;
