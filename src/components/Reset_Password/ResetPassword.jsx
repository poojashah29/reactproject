import React, { Component } from "react";
import { Row, Col, Input } from "antd";
import Footer from "../Footer/Footer.jsx";
import NavbarComponent from "../Navbar/NavbarComponent.jsx";

class ResetPassword extends Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {}
    };
    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(
      this
    );
  }
  redirectPage() {
    window.location.hash = "/GetNewPassword";
  }
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  submituserRegistrationForm(e) {
    e.preventDefault();
    if (this.validateForm()) {
      this.redirectPage();
      let fields = {};
      fields["emailid"] = "";
      this.setState({ fields: fields });
    }
  }

  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["emailid"]) {
      formIsValid = false;
      errors["emailid"] = "Please enter your email";
    }

    if (typeof fields["emailid"] !== "undefined") {
      //regular expression for email validation
      var pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(fields["emailid"])) {
        formIsValid = false;
        errors["emailid"] = "Please enter a valid email";
      }
    }

    this.setState({ errors: errors });

    return formIsValid;
  }

  render() {
    return (
      <div id="ResetPassword">
        <div className="LoginBackground">
          <NavbarComponent />
          <div className="modal-dialog">
            <div className="modal-content setPos_Model">
              <div className="modal-body">
                <form>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <h2>Password Reset</h2>
                    </Col>
                  </Row>
                  <Row className="rowMargin">
                    <Col span={18} offset={3}>
                      <span className="inputfield-heading-color">Email</span>
                      <Input
                        placeholder="Enter Email"
                        type="email"
                        autoComplete="off"
                        name="emailid"
                        size="large"
                        className="inputBackground"
                        value={this.state.fields.emailid}
                        onChange={this.handleChange}
                      />
                      <div className="errorMsg">
                        {this.state.errors.emailid}
                      </div>
                    </Col>
                  </Row>
                  <div id="contactBtn">
                    <button
                      type="submit"
                      id="btn_getpassword"
                      onClick={this.submituserRegistrationForm}
                    >
                      Get New Password
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default ResetPassword;
