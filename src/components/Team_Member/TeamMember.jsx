import React, { Component } from "react";

import { Upload, Form, Input, Icon, Button, Row, Col } from "antd";
import Footer from "../Footer/Footer.jsx";
import Inner_Navbar from "../Navbar/Inner_Navbar";

const FormItem = Form.Item;
let uuid = 0;

const props = {
  action: "//jsonplaceholder.typicode.com/posts/",
  onChange({ file, fileList }) {
    if (file.status !== "uploading") {
      console.log(file, fileList);
    }
  },
  defaultFileList: []
};

class TeamMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "xyz",
      email: "zyz@gmail.com",
      position: "co-founder",
      bio: "xyz is co-founder"
    };
  }

  remove = k => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue("keys");
    // We need at least one passenger
    if (keys.length === 1) {
      return;
    }

    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k)
    });
  };

  componentDidMount() {
    this.add();
  }

  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue("keys");
    const nextKeys = keys.concat(uuid);
    uuid++;
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue({
      keys: nextKeys
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        window.location.hash = "/MyProfile";
      }
    });
  };
  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 24 }
      }
    };
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 24, offset: 0 }
      }
    };
    getFieldDecorator("keys", { initialValue: [] });
    const keys = getFieldValue("keys");
    const formItems = keys.map((k, index) => {
      return (
        <FormItem
          {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
          required={false}
          key={k}
        >
          {getFieldDecorator(`names[${k}]`, {
            validateTrigger: ["onChange", "onBlur"],
            rules: [
              {
                required: true,
                whitespace: true,
                message: "Fields can not be empty"
              }
            ]
          })(
            <div>
              <Row>
                <Col span={7} offset={1} className="team-member">
                  <span className="inputfield-heading-color">Name</span>
                  <Input placeholder="Enter name" className="inputBackground" />
                </Col>
                <Col span={6} offset={1} className="team-member">
                  <span className="inputfield-heading-color">Email</span>
                  <Input
                    placeholder="Enter Email"
                    className="inputBackground"
                  />
                </Col>
                <Col span={7} offset={1} className="team-member">
                  <span className="inputfield-heading-color">Position</span>
                  <Input
                    placeholder="Enter Position"
                    className="inputBackground"
                  />
                </Col>
              </Row>
              <Row>
                <Col span={22} offset={1} className="team-member">
                  <span className="inputfield-heading-color">Bio</span>
                  <textarea
                    className="form-control inputBackground"
                    rows="5"
                    id="comment"
                  />
                </Col>
              </Row>
              <Upload {...props}>
                <Button id="uploadImage">
                  <Icon type="upload" /> Upload Image
                </Button>
              </Upload>
              <Row>
                <Col span={22} offset={1}>
                  <div id="delete">
                    <Icon
                      className="dynamic-delete-button"
                      type="close"
                      onClick={
                        () => this.remove(k) // disabled={keys.length === 1}
                      }
                    />
                    <span>Delete</span>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col span={22} offset={1} className="memberBorder" />
              </Row>
            </div>
          )}
        </FormItem>
      );
    });

    return (
      <div id="teamMember">
        <div className="innerBackground bg_imgpadding">
          <Inner_Navbar />
        </div>
        <div className="modal-dialog" id="modal">
          <div className="modal-content">
            <div className="modal-body" />
            <Row>
              <Col offset={1}>
                <h4 className="headingStyle">Add Team Members</h4>
              </Col>
            </Row>
            <Form onSubmit={this.handleSubmit}>
              {formItems}
              <Row>
                <Col offset={1}>
                  <FormItem {...formItemLayoutWithOutLabel}>
                    <Button
                      onClick={this.add}
                      style={{ border: "none", color: "#1890ff" }}
                    >
                      <Icon type="plus" /> Add
                    </Button>
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col offset={1}>
                  <FormItem {...formItemLayoutWithOutLabel}>
                    <Button type="primary" htmlType="submit">
                      Create
                    </Button>
                  </FormItem>
                </Col>
              </Row>
            </Form>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
export default Form.create()(TeamMember);
