import React, { Component } from "react";
import gallery_img1 from "../../images/gallery_img1.png";
import gallery_img2 from "../../images/gallery_img2.png";
import gallery_img3 from "../../images/gallery_img3.png";
import "../../style/App.css";

class MiddleWare extends Component {
  render() {
    return (
      <div id="MiddleWare">
        <div className="container">
          <h2 className="midH2">
            Solutions for scaling startup to running business
          </h2>
          <div className="row">
            <div className="col-sm-6 col-md-4 col-lg-4">
              <div className="midimgBorder">
                <img className="midImgWidth" src={gallery_img1} alt="" />
                <div className="caption">
                  <h4>Shared Services</h4>
                  <p>
                    Centralize communications, measure efficiency &amp; automate
                    tasks.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-md-4 col-lg-4">
              <div className="midimgBorder">
                <img className="midImgWidth" src={gallery_img2} alt="" />
                <div className="caption">
                  <h4>Financial Services</h4>
                  <p>
                    Speed-up approvals using a scalable, robust, and secure
                    plateform
                  </p>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-md-4 col-lg-4">
              <div className="midimgBorder">
                <img className="midImgWidth" src={gallery_img3} alt="" />
                <div className="caption">
                  <h4>Innovation</h4>
                  <p>Promote cross-functional innovation in your enterprise</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container-fluid" className="midContainer">
          <form className="midForm">
            <span>
              Collaboration, services integration features make it the leading
              <br />
              solution for any start-ups of all sizes
            </span>
          </form>
        </div>
      </div>
    );
  }
}

export default MiddleWare;
