import React, { Component } from "react";
import "../../../src/index.css";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import MiddleWare from "./MiddleWare";

class HomePage extends Component {
  render() {
    return (
      <div>
        <Header />
        <MiddleWare />
        <Footer />
      </div>
    );
  }
}

export default HomePage;
