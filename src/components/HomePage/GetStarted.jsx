import React, { Component } from "react";
import "../../style/App.css";

class GetStarted extends Component {
  handleClick() {
    window.location.hash = "/SignUp";
  }
  render() {
    return (
      <div className="container content_mg">
        <h1>The operations excellence platform.</h1>
        <h6>
          We help teams to create and run efficient processes in our fast,
          <br />
          intuitive and powerful platform.
        </h6>

        <button
          type="button"
          className="btn_mng btn btn-primary"
          onClick={this.handleClick.bind(this)}
        >
          GET STARTED
        </button>
      </div>
    );
  }
}

export default GetStarted;
