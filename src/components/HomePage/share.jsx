import React, { Component } from "react";
import facebook from "../../icons/Login/fb.png";
import google from "../../icons/Login/go.png";
import linkdin from "../../icons/Login/in.png";
import twitter from "../../icons/Login/tw.png";

class Share extends Component {
  state = {};

  render() {
    return (
      <div id="share" className="grid">
        <div className="form-group text_align col-xs-9 alignment">
          <span> Or, Create an Account with: </span>
        </div>
        <div className="register col-xs-9 alignment">
          <div className="row">
            <div className="col-md-12 register">
              <a href="https://www.facebook.com/" target="_blank">
                <img className="iconstyle_1login" src={facebook} alt="" />
              </a>
              <a href="https://twitter.com/" target="_blank">
                <img className="iconstylelogin" src={twitter} alt="" />
              </a>
              {/* <a href="https://www.google.com/" target="_blank">
                <img
                  className="iconstylelogin"
                  src={google}
                  style={{ display: "none" }}
                  alt=""
                />
              </a> */}

              <a href="https://www.linkedin.com/" target="_blank">
                <img className="iconstylelogin" src={linkdin} alt="" />
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Share;
