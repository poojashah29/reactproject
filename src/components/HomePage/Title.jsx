import React, { Component } from "react";
import icon from "../../images/icon.png";
import { Link } from "react-router";

class Title extends Component {
  state = {};
  render() {
    return (
      <div className="title">
        <Link to="/">
          <img src={icon} alt="" />
        </Link>
      </div>
    );
  }
}

export default Title;
