import React, { Component } from "react";
import "../index.css";
import HomePage from "./HomePage/HomePage";
import SignIn from "./Registeration/SignIn";
import SignUp from "./Registeration/SignUp";
import ResetPassword from "./Reset_Password/ResetPassword";
import GetNewPassword from "./Reset_Password/GetNewPassword";
import CompanyProfile from "../components/Company_Profile/CompanyProfile";
import TeamMember from "./Team_Member/TeamMember";
import MyProfile from "../components/Employee_Profile/MyProfile";
import AddCompanies from "../components/Company_Profile/AddCompanies";
import CreateMyProfile from "../components/Employee_Profile/CreateMyProfile";
import MyCompanyProfile from "../components/Company_Profile/MyCompanyProfile";
import Questions from "./Questionaire/Questions";

import EditCompanyProfile from "../components/Company_Profile/EditCompanyProfile";
import EditMyProfile from "../components/Employee_Profile/EditMyProfile";

import Error from "./Error/Error";
import { Router, Route, hashHistory } from "react-router";

class App extends Component {
  render() {
    return (
      <Router history={hashHistory}>
        <Route path="/" component={HomePage} />
        <Route path="/signin" component={SignIn} />
        <Route path="/signup" component={SignUp} />
        <Route path="/ResetPassword" component={ResetPassword} />
        <Route path="/GetNewPassword" component={GetNewPassword} />
        <Route path="/CompanyProfile" component={CompanyProfile} />
        <Route path="/TeamMember" component={TeamMember} />
        <Route path="/MyProfile" component={MyProfile} />
        <Route path="/AddCompanies" component={AddCompanies} />
        <Route path="/CreateMyProfile" component={CreateMyProfile} />
        <Route path="/MyCompanyProfile" component={MyCompanyProfile} />
        <Route path="/Questions" component={Questions} />

        <Route path="/EditCompanyProfile" component={EditCompanyProfile} />
        <Route path="/EditMyProfile" component={EditMyProfile} />

        <Route path="/error" component={Error} />
      </Router>
    );
  }
}

export default App;
