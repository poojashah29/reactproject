import React, { Component } from "react";
import { Progress, Row, Col } from "antd";
import OvalEmpty from "../../images/companyprofile/Oval-empty.png";
import bounds from "../../images/companyprofile/bounds.png";

class Strategy extends Component {
  constructor(props) {
    super(props);

    this.state = {
      companyTitle: "Strategy",
      companyDescription:
        "Advanced Computer Vision and AI for Augumented Reality",
      percentage: 67
    };
  }
  render() {
    return (
      <div className="modal-dialog marketingStyle">
        <div className="modal-content">
          <div className="modal-body">
            <form style={{ textAlign: "left" }}>
              <Row>
                <Col span={4}>
                  <div>
                    <h4 className="companyTitleNum">4</h4>
                    <img src={bounds} alt="" />
                  </div>
                </Col>
                <Col span={18}>
                  <h2 className="Companytitle">{this.state.companyTitle}</h2>
                  <p style={{ fontSize: "13px" }}>
                    {this.state.companyDescription}
                  </p>

                  <Row style={{ paddingTop: "60px" }}>
                    <Progress percent={this.state.percentage} />
                  </Row>
                </Col>
                <Col span={2} style={{ bottom: "3px" }}>
                  <img src={OvalEmpty} style={{ float: "right" }} alt="" />
                </Col>
              </Row>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Strategy;
