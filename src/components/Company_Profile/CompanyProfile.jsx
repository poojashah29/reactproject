import React, { Component } from "react";
import { Row, Col, Input, Select } from "antd";

import Footer from "../Footer/Footer";
import Inner_Navbar from "../Navbar/Inner_Navbar";
const Option = Select.Option;

//company Profile Page
class CompanyProfile extends Component {
  //Constructor
  constructor() {
    super();

    // default state
    this.state = { fields: {}, errors: {} };
    this.handleChange = this.handleChange.bind(this);
    this.createCompanyProfile = this.createCompanyProfile.bind(this);
  }

  //Redirect page to the given path
  redirectPage() {
    window.location.hash = "/CreateMyProfile";
  }

  //Call on field Change
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  //On Drop Down Menu change
  handleDropDownChange(value) {
    console.log(`selected ${value}`);
  }

  //Validate Form and Reset Form fields
  createCompanyProfile(e) {
    e.preventDefault();
    if (this.validateForm()) {
      this.redirectPage();
      let fields = {};
      fields["email"] = "";
      fields["phoneNumber"] = "";
      fields["address"] = "";
      fields["entityName"] = "";
      fields["contactName"] = "";
      fields["business"] = "";
      //set state
      this.setState({
        fields: fields
      });
    }
  }

  //phone validation
  validatePhone() {
    var phone = document.getElementById("phone");
    var RE = /^[\d\.\-]+$/;
    if (!RE.test(phone.value)) {
      return false;
    }
    return true;
  }

  //fields validation
  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    //   phone number validation
    if (this.validatePhone() === false) {
      formIsValid = false;
      errors["phoneNumber"] = "Please enter a valid phone number";
    } else if (fields["phoneNumber"].length !== 10) {
      formIsValid = false;
      errors["phoneNumber"] = "Phone number should be 10 digits";
    }

    //email validation
    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "Please enter your email";
    }
    if (typeof fields["email"] !== "undefined") {
      //regular expression for email validation
      var pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(fields["email"])) {
        formIsValid = false;
        errors["email"] = "Please enter a valid email";
      }
    }

    //address validation
    if (!fields["address"]) {
      formIsValid = false;
      errors["address"] = "Please enter address";
    }
    //city required
    if (!fields["city"]) {
      formIsValid = false;
      errors["city"] = "Please enter city";
    }
    //state required
    if (!fields["state"]) {
      formIsValid = false;
      errors["state"] = "Please enter state";
    }
    //zip code required
    if (!fields["zip"]) {
      formIsValid = false;
      errors["zip"] = "Please enter zip";
    }

    //entity validation
    if (!fields["entityName"]) {
      formIsValid = false;
      errors["entityName"] = "Please enter entity name";
    }

    //contactName validation
    if (!fields["contactName"]) {
      formIsValid = false;
      errors["contactName"] = "Please enter contact name";
    }
    this.setState({ errors: errors });

    return formIsValid;
  }

  render() {
    return (
      <div id="CompanyProfile">
        <div className="innerBackground bg_imgpadding">
          <Inner_Navbar />
        </div>
        <div className="modal-dialog" id="modal">
          <div className="modal-content">
            <div className="modal-body" />
            <form>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <h3 className="headingStyle">
                    Create&nbsp;Your&nbsp;Company&nbsp;Profile
                  </h3>
                </Col>
              </Row>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <span className="inputfield-heading-color-companyProfile">
                    Legal Entity Name
                  </span>
                  <Input
                    placeholder="Entity Name"
                    type="email"
                    name="entityName"
                    autoComplete="off"
                    size="large"
                    className="inputBackground"
                    value={this.state.fields.entityName}
                    onChange={this.handleChange}
                  />
                  <div className="errorMsg">{this.state.errors.entityName}</div>
                </Col>
              </Row>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <span className="inputfield-heading-color-companyProfile">
                    Contact Name. / Title
                  </span>
                  <Input
                    placeholder="Contact Name"
                    type="text"
                    name="contactName"
                    autoComplete="off"
                    size="large"
                    className="inputBackground"
                    value={this.state.fields.contactName}
                    onChange={this.handleChange}
                  />
                  <div className="errorMsg">
                    {this.state.errors.contactName}
                  </div>
                </Col>
              </Row>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <span className="inputfield-heading-color-companyProfile">
                    Doing Business As <span>(optional)</span>
                  </span>
                  <Input
                    placeholder="Trade Name or DBA"
                    type="text"
                    name="business"
                    autoComplete="off"
                    size="large"
                    className="inputBackground"
                    value={this.state.fields.business}
                    onChange={this.handleChange}
                  />
                  <div className="errorMsg">{this.state.errors.business}</div>
                </Col>
              </Row>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <span className="inputfield-heading-color-companyProfile">
                    Industry
                  </span>

                  <Select
                    className="inputBackground required"
                    defaultValue="Trade 1"
                    id="dropdown_trade"
                    size="large"
                    onChange={this.handleDropDownChange}
                  >
                    <Option value="T1">Trade 1</Option>
                    <Option value="T2">Trade 2</Option>
                    <Option value="T3">Trade 3</Option>
                    <Option value="T4">Trade 4</Option>
                  </Select>
                </Col>
              </Row>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <span className="inputfield-heading-color-companyProfile">
                    State of Incorporation
                  </span>
                  <Select
                    className="inputBackground"
                    defaultValue="State One"
                    id="dropdown_state"
                    size="large"
                    onChange={this.handleDropDownChange}
                  >
                    <Option value="S1">State One</Option>
                    <Option value="S2">State Three</Option>
                    <Option value="S3">State Three</Option>
                    <Option value="S4">State Four</Option>
                  </Select>
                </Col>
              </Row>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <span className="inputfield-heading-color-companyProfile">
                    Address
                  </span>
                  <Input
                    placeholder="Enter Address"
                    type="text"
                    name="address"
                    autoComplete="off"
                    size="large"
                    className="inputBackground"
                    value={this.state.fields.address}
                    onChange={this.handleChange}
                  />
                  <div className="errorMsg">{this.state.errors.address}</div>
                </Col>
              </Row>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <span className="inputfield-heading-color-companyProfile">
                    City
                  </span>
                  <Input
                    placeholder="Enter City"
                    type="text"
                    name="city"
                    autoComplete="off"
                    size="large"
                    className="inputBackground"
                    value={this.state.fields.city}
                    onChange={this.handleChange}
                  />
                  <div className="errorMsg">{this.state.errors.city}</div>
                </Col>
              </Row>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <span className="inputfield-heading-color-companyProfile">
                    State
                  </span>
                  <Input
                    placeholder="Enter State"
                    type="text"
                    name="state"
                    autoComplete="off"
                    size="large"
                    className="inputBackground"
                    value={this.state.fields.state}
                    onChange={this.handleChange}
                  />
                  <div className="errorMsg">{this.state.errors.state}</div>
                </Col>
              </Row>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <span className="inputfield-heading-color-companyProfile">
                    Zip Code
                  </span>
                  <Input
                    placeholder="Enter Zip Code"
                    type="text"
                    name="zip"
                    autoComplete="off"
                    size="large"
                    className="inputBackground"
                    value={this.state.fields.zip}
                    onChange={this.handleChange}
                  />
                  <div className="errorMsg">{this.state.errors.zip}</div>
                </Col>
              </Row>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <span className="inputfield-heading-color-companyProfile">
                    Email
                  </span>
                  <Input
                    placeholder="Enter Email"
                    type="email"
                    name="email"
                    autoComplete="off"
                    size="large"
                    className="inputBackground"
                    value={this.state.fields.email}
                    onChange={this.handleChange}
                  />
                  <div className="errorMsg">{this.state.errors.email}</div>
                </Col>
              </Row>
              <Row className="rowMargin">
                <Col span={18} offset={3}>
                  <span className="inputfield-heading-color-companyProfile">
                    Phone
                  </span>
                  <Input
                    placeholder="Enter Phone Number"
                    type="text"
                    name="phoneNumber"
                    id="phone"
                    autoComplete="off"
                    size="large"
                    className="inputBackground"
                    value={this.state.fields.phoneNumber}
                    onChange={this.handleChange}
                  />
                  <div className="errorMsg">
                    {this.state.errors.phoneNumber}
                  </div>
                </Col>
              </Row>
              <Row>
                <Col span={18} offset={3}>
                  <button
                    type="submit"
                    className="btn_createCompany"
                    onClick={this.createCompanyProfile}
                  >
                    Create
                  </button>
                </Col>
              </Row>
            </form>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default CompanyProfile;
