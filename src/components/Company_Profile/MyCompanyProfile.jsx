import React, { Component } from "react";
import Footer from "../Footer/Footer";
import { Progress, Row, Col } from "antd";

import Oval from "../../images/companyprofile/Oval.png";
import Inner_Navbar from "../Navbar/Inner_Navbar";
import Idea from "../../components/Marketing_Workflow/Idea";
import Plan from "../../components/Marketing_Workflow/Plan";
import Legal from "../../components/Marketing_Workflow/Legal";
import CRM from "../../components/Marketing_Workflow/CRM";
import Analytics from "../../components/Marketing_Workflow/Analytics";
import Accounting from "../../components/Marketing_Workflow/Accounting";
import Strategy from "../../components/Marketing_Workflow/Strategy";
import Team from "../../components/Marketing_Workflow/Team";
import Website_and_Branding from "../../components/Marketing_Workflow/Website_and_Branding";
import Document from "../../components/My_Documents/Document";
import add from "../../images/companyprofile/plus_sign.png";
import download from "../../images/myprofile/download.png";
import share from "../../images/myprofile/share.png";
import edit from "../../images/myprofile/edit.png";
import doc2 from "../../images/companyprofile/doc2.png";
import EmployeeSlider from "./employeeSlider";
import { SimpleShareButtons } from "react-simple-share";

//Parent (Add button) component, Onclick add new child (Document) component
const ParentComponent = props => (
  <div>
    <div id="children-pane">{props.children}</div>
    <a onClick={props.addChild} target="blank">
      <div className="col-md-3 col-sm-4 col-xs-12 addCompany_btn">
        <img src={add} alt="" />
      </div>
    </a>
  </div>
);

class MyCompanyProfile extends Component {
  constructor(props) {
    super(props);

    //default state
    this.state = {
      numDocuments: 0,
      visible: false,
      percentage: 80,
      shareclick: false,
      showdocs: false,
      profileImg: Oval,
      name: "Vertical Ai",
      position: "The future of new Ai",
      email: "hello@ai.com",
      bio:
        "Maecenas sed diam aget risus varius blandit sit amet non magna.Integer posuere erat a ante venentis dapibus posuere velit aliquet.Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor."
    };
  }

  onShareProfileClick = () => {
    this.setState({ shareclick: !this.state.shareclick });
  };

  //set Document state visible or not visible
  showDocuments = () => {
    this.setState({ showdocs: !this.state.showdocs });
  };
  //increament each time new Document add
  onAddChild = () => {
    this.setState({
      numDocuments: this.state.numDocuments + 1
    });
  };

  hide = () => {
    this.setState({
      visible: false
    });
  };

  handleVisibleChange = visible => {
    this.setState({ visible });
  };

  render() {
    //list of Documents added
    const children = [];

    //push all documents in children[]
    for (var i = 0; i < this.state.numDocuments; i += 1) {
      children.push(<Document key={i} number={i} />);
    }

    return (
      <div id="addcompany">
        <div className="innerBackground bg_imgpadding">
          <Inner_Navbar />
        </div>
        <div className="modal-dialog" id="my_company_modal">
          <EmployeeSlider />
          <h4>MY COMPANY</h4>
          <div className="modal-content">
            <div className="modal-body" />
            <form>
              {/* documents, download, share, edit bar */}
              <div className="row">
                <div className="col-sm-4" className="sharecompanyprofile ">
                  <a id="edit" href="#/EditCompanyProfile">
                    <img src={edit} alt="" style={{ paddingLeft: "7px" }} />
                    <span>EDIT</span>
                  </a>
                </div>
                <div className="col-sm-4" className="sharecompanyprofile ">
                  <a
                    id="share"
                    onClick={this.onShareProfileClick.bind(this)}
                    target="_blank"
                  >
                    <img src={share} alt="" style={{ paddingLeft: "10px" }} />
                    <span>SHARE</span>
                  </a>
                  {this.state.shareclick === true ? (
                    <SimpleShareButtons whitelist={["LinkedIn"]} />
                  ) : null}
                </div>
                <div className="col-sm-4" className="sharecompanyprofile ">
                  <a
                    id="download"
                    href="https://s3.amazonaws.com/rd-downloads-01/Rich-Dad-Poor-Dad-eBook.pdf"
                    target="_blank"
                    download
                  >
                    <img
                      src={download}
                      alt=""
                      style={{ paddingLeft: "30px" }}
                    />
                    <span>DOWNLOAD</span>
                  </a>
                </div>
                <div className="col-sm-4" className="sharecompanyprofile ">
                  <a
                    id="documents"
                    onClick={this.showDocuments.bind(this)}
                    target="_blank"
                  >
                    <img src={doc2} alt="" style={{ paddingLeft: "30px" }} />
                    <span>DOCUMENTS</span>
                  </a>
                </div>
              </div>
              {/* profile picture, Name Email etc..... */}
              <div className="row" id="compprofile">
                <div className="col col-lg-2 compprofileimg">
                  <img src={this.state.profileImg} alt="" />
                </div>
                <div
                  className="col col-lg-10"
                  style={{ paddingLeft: "0px", zIndex: "-1" }}
                >
                  <div className="row">
                    <div className="col">
                      <h3 id="comp_name">{this.state.name}</h3>
                    </div>
                    <div className="col">
                      <span id="comp_position">{this.state.position}</span>
                    </div>
                    <div className="col">
                      <span id="comp_email">{this.state.email}</span>
                    </div>
                    <div className="col" id="comp_bio">
                      <div>
                        <div
                          className="col col-sm-10"
                          style={{ paddingLeft: "0px" }}
                        >
                          <span>{this.state.bio}</span>
                        </div>
                        <div className="col col-sm-2">
                          <Progress
                            type="circle"
                            percent={this.state.percentage}
                          />
                          <span id="complete_profile">Profile Complete</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        {this.state.showdocs === true ? (
          <div>
            <Row>
              <Col>
                <h4 className="companyheadingStyle">MY DOCUMENTS</h4>
              </Col>
            </Row>
            <div className="row" id="profile-popup">
              <ParentComponent addChild={this.onAddChild}>
                {children}
              </ParentComponent>
            </div>
          </div>
        ) : null}

        <Row>
          <Col>
            <h4 className="companyheadingStyle">MY MARKETING WORKFLOW</h4>
          </Col>
        </Row>
        <div className="row" id="profile-popup">
          <a href="#/Questions" style={{ color: "black" }}>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <Idea />
            </div>
          </a>
          <a href="#/Questions" style={{ color: "black" }}>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <Plan />
            </div>
          </a>
          <a href="#/Questions" style={{ color: "black" }}>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <Legal />
            </div>
          </a>
          <a href="#/Questions" style={{ color: "black" }}>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <Strategy />
            </div>
          </a>
          <a href="#/Questions" style={{ color: "black" }}>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <Website_and_Branding />
            </div>
          </a>
          <a href="#/Questions" style={{ color: "black" }}>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <CRM />
            </div>
          </a>
          <a href="#/Questions" style={{ color: "black" }}>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <Analytics />
            </div>
          </a>
          <a href="#/Questions" style={{ color: "black" }}>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <Team />
            </div>
          </a>
          <a href="#/Questions" style={{ color: "black" }}>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <Accounting />
            </div>
          </a>
        </div>

        <Footer />
      </div>
    );
  }
}

export default MyCompanyProfile;
