import React, { Component } from "react";
import add from "../../images/myprofile/plus_sign.png";

//Button to create new company
class AddCompanies extends Component {
  render() {
    return (
      <div className="modal-dialog" id="my_companies_modal">
        <div className="modal-content">
          <div className="modal-body">
            <form>
              <img src={add} alt="" style={{ marginLeft: "38%" }} />
              <span>Add Business Profile</span>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default AddCompanies;
