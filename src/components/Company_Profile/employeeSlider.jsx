import React, { Component } from "react";
import E1 from "../../images/companyprofile/E1.png";
import E2 from "../../images/companyprofile/E2.png";
import E3 from "../../images/companyprofile/E3.png";
import { Progress, Row, Col } from "antd";
import arrow from "../../images/companyprofile/arrow.png";

class EmployeeSlider extends Component {
  render() {
    return (
      <Row id="slider">
        <Col span={2} style={{ paddingTop: "12px" }}>
          <a>
            <img src={arrow} alt="" />
          </a>
        </Col>
        <Col span={7}>
          <Row className="slider-border">
            <Col span={6} className="sliderMob">
              <img src={E1} alt="" />
            </Col>
            <Col span={16} offset={2}>
              <h3>Jared Anderson</h3>
              <span>Co-Founder</span>
            </Col>
          </Row>
        </Col>
        <Col span={7}>
          <Row className="slider-border">
            <Col span={6} className="sliderMob">
              <img src={E2} alt="" />
            </Col>
            <Col span={16} offset={2}>
              <h3>Elmer Moreno</h3>
              <span>Co-Founder</span>
            </Col>
          </Row>
        </Col>
        <Col span={8}>
          <Row>
            <Col span={6} className="sliderMob">
              <img src={E3} alt="" />
            </Col>
            <Col span={16} offset={2}>
              <h3>Earl Malone</h3>
              <span>Co-Founder</span>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default EmployeeSlider;
