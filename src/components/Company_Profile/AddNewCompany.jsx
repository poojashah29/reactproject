import React, { Component } from "react";
import { Progress, Row, Col } from "antd";
import companyLogo from "../../images/myprofile/char.png";

//Create New Company in My Profile
class AddNewCompany extends Component {
  constructor(props) {
    super(props);
    // default state
    this.state = {
      companyTitle: "Vertical AI",
      companyDescription:
        "Advanced Computer Vision and AI for Augumented Reality",
      percentage: 67
    };
  }
  render() {
    return (
      <a href="#/MyCompanyProfile" style={{ color: "black" }}>
        <div className="col-md-4 col-sm-6 col-xs-12">
          <div className="modal-dialog marketingStyle">
            <div className="modal-content">
              <div className="modal-body">
                <form style={{ textAlign: "left", paddingLeft: "20px" }}>
                  <Row>
                    <Col span={18}>
                      <h2 className="Companytitle">
                        {this.state.companyTitle}
                      </h2>
                    </Col>
                    <Col span={6}>
                      <img
                        src={companyLogo}
                        style={{ float: "right" }}
                        alt=""
                      />
                    </Col>
                  </Row>
                  <div>
                    <p>{this.state.companyDescription}</p>
                  </div>
                  <Row style={{ paddingTop: "54px" }}>
                    <Col span={21}>
                      <Progress type="line" percent={this.state.percentage} />
                    </Col>
                  </Row>
                </form>
              </div>
            </div>
          </div>
        </div>
      </a>
    );
  }
}

export default AddNewCompany;
