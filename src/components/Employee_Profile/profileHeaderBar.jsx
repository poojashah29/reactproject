import React, { Component } from "react";
import download from "../../images/myprofile/download.png";
import share from "../../images/myprofile/share.png";
import edit from "../../images/myprofile/edit.png";
import { SimpleShareButtons } from "react-simple-share";
class profileHeaderBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shareclick: false
    };
  }

  onShareProfileClick = () => {
    this.setState({ shareclick: !this.state.shareclick });
  };
  render() {
    return (
      <div className="row">
        <div className="col-sm-4" className="shareprofile">
          <a id="edit" href="#/EditMyProfile">
            <img src={edit} alt="" style={{ paddingLeft: "7px" }} />
            <span>EDIT</span>
          </a>
        </div>
        <div className="col-sm-4" className="shareprofile">
          <a
            id="share"
            onClick={this.onShareProfileClick.bind(this)}
            target="_blank"
          >
            <img src={share} alt="" style={{ paddingLeft: "10px" }} />
            <span>SHARE</span>
          </a>
          {this.state.shareclick === true ? (
            <SimpleShareButtons
              whitelist={["Facebook", "Twitter", "Google+"]}
            />
          ) : null}
        </div>
        <div className="col-sm-4" className="shareprofile">
          <a
            id="download"
            href="https://s3.amazonaws.com/rd-downloads-01/Rich-Dad-Poor-Dad-eBook.pdf"
            target="_blank"
            download="proposed_file_name"
          >
            <img src={download} alt="" style={{ paddingLeft: "30px" }} />
            <span>DOWNLOAD</span>
          </a>
        </div>
      </div>
    );
  }
}

export default profileHeaderBar;
