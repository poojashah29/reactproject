import React, { Component } from "react";
import Footer from "../Footer/Footer";
import Inner_Navbar from "../Navbar/Inner_Navbar";
import { Row, Col } from "antd";
import Oval from "../../images/myprofile/Oval.png";
import AddCompanies from "../Company_Profile/AddCompanies";
import AddNewCompany from "../Company_Profile/AddNewCompany";
import ProfileHeaderBar from "./profileHeaderBar";

//Add component to create new company in mt profile
const ParentComponent = props => (
  <div>
    <div id="children-pane">
      {props.children}
      <a onClick={props.addChild}>
        <div className="col-md-4 col-sm-6 col-xs-12" id="add">
          <AddCompanies />
        </div>
      </a>
    </div>
  </div>
);

class MyProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numCompanies: 0,
      profileImg: Oval,
      name: "Jared Anderson",
      position: "Co-Founder",
      email: "Jared@newco.com",
      bio:
        "Maecenas sed diam aget risus varius blandit sit amet non magna. Integer posuere erat a ante venentis dapibus posuere velit aliquet. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor."
    };
  }
  onAddChild = () => {
    this.setState({
      numCompanies: this.state.numCompanies + 1
    });
  };

  render() {
    console.log("children");
    const children = [];

    for (var i = 0; i < this.state.numCompanies; i += 1) {
      children.push(<AddNewCompany key={i} number={i} />);
    }

    return (
      <div id="addcompany">
        <div className="innerBackground bg_imgpadding">
          <Inner_Navbar />
        </div>

        <div className="modal-dialog" id="my_profile_modal">
          {/* ------------------------------------------------ */}
          <h4>MY PROFILE</h4>
          <div className="modal-content">
            <div className="modal-body" />
            <form>
              {/* edit, share, download */}
              <ProfileHeaderBar />

              <div className="row" id="profile">
                <div className="col col-lg-2 profileimg">
                  <img src={this.state.profileImg} alt="" />
                </div>
                <div
                  className="col col-lg-10"
                  style={{ paddingLeft: "0px", zIndex: "-1" }}
                >
                  <div className="row">
                    <div className="col">
                      <h3 id="Emp_name">{this.state.name}</h3>
                    </div>
                    <div className="col">
                      <span id="Emp_position">{this.state.position}</span>
                    </div>
                    <div className="col">
                      <span id="Emp_email">{this.state.email}</span>
                    </div>
                    <div className="col" id="Emp_bio">
                      <span>{this.state.bio}</span>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <Row>
          <Col>
            <h4 className="companyheadingStyle">MY COMPANIES</h4>
          </Col>
        </Row>
        <div className="row" id="profile-popup">
          <ParentComponent id="hello" addChild={this.onAddChild}>
            {children}
          </ParentComponent>
        </div>
        <Footer />
      </div>
    );
  }
}

export default MyProfile;
