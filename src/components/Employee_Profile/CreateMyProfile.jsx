import React, { Component } from "react";
import { Upload, Form, Input, Icon, Button, Row, Col } from "antd";
import Footer from "../Footer/Footer";
import Inner_Navbar from "../Navbar/Inner_Navbar";

//image uploading
const props = {
  action: "//jsonplaceholder.typicode.com/posts/",
  onChange({ file, fileList }) {
    if (file.status !== "uploading") {
      console.log(file, fileList);
    }
  },
  defaultFileList: []
};

class CreateMyProfile extends Component {
  constructor() {
    super();
    //default state
    this.state = {
      fields: {},
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(
      this
    );
  }

  //redirect page to given URL
  redirectPage() {
    window.location.hash = "/TeamMember";
  }
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });
  }

  //validate form and set field to empty
  submituserRegistrationForm(e) {
    e.preventDefault();
    if (this.validateForm()) {
      this.redirectPage();
      let fields = {};
      fields["emailid"] = "";
      fields["name"] = "";
      fields["position"] = "";

      this.setState({ fields: fields });
    }
  }

  //validate form
  validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    //email validation
    if (!fields["emailid"]) {
      formIsValid = false;
      errors["emailid"] = "Please enter your email";
    }

    if (typeof fields["emailid"] !== "undefined") {
      //regular expression for email validation
      var pattern = new RegExp(
        /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
      );
      if (!pattern.test(fields["emailid"])) {
        formIsValid = false;
        errors["emailid"] = "Please enter valid email";
      }
    }

    //name is required
    if (!fields["name"]) {
      formIsValid = false;
      errors["name"] = "Please enter your name";
    }
    this.setState({ errors: errors });

    return formIsValid;
  }

  render() {
    return (
      <div id="teamMember">
        <div className="innerBackground bg_imgpadding">
          <Inner_Navbar />
        </div>
        <div className="modal-dialog" id="modal">
          <div className="modal-content">
            <div className="modal-body" />
            <Row>
              <Col offset={1}>
                <h4 className="headingStyle">Create Your Profile</h4>
              </Col>
            </Row>
            <Form>
              <div>
                <Row>
                  <Col span={7} offset={1} className="team-member">
                    <span className="inputfield-heading-color">Name</span>
                    <Input
                      placeholder="Enter name"
                      className="inputBackground"
                      id="name"
                      name="name"
                      value={this.state.fields.name}
                      onChange={this.handleChange}
                    />
                    <div className="Profile-errorMsg">
                      {this.state.errors.name}
                    </div>
                  </Col>
                  <Col span={6} offset={1} className="team-member">
                    <span className="inputfield-heading-color">Email</span>
                    <Input
                      placeholder="Enter Email"
                      className="inputBackground"
                      id="emailid"
                      name="emailid"
                      value={this.state.fields.emailid}
                      onChange={this.handleChange}
                    />
                    <div className="Profile-errorMsg">
                      {this.state.errors.emailid}
                    </div>
                  </Col>
                  <Col span={7} offset={1} className="team-member">
                    <span className="inputfield-heading-color">Position</span>
                    <Input
                      placeholder="Enter Position"
                      className="inputBackground"
                      id="position"
                      name="position"
                      value={this.state.fields.position}
                      onChange={this.handleChange}
                    />
                    <div className="Profile-errorMsg">
                      {this.state.errors.position}
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col span={22} offset={1} className="team-member">
                    <span className="inputfield-heading-color">Bio</span>
                    <textarea
                      className="form-control inputBackground"
                      rows="5"
                      id="comment"
                    />
                  </Col>
                </Row>

                <div>
                  <Upload {...props}>
                    <Button id="uploadImage">
                      <Icon type="upload" /> Upload Image
                    </Button>
                  </Upload>
                </div>
              </div>
              <Row>
                <Col offset={1}>
                  <button
                    type="submit"
                    className="btn_createCompany"
                    onClick={this.submituserRegistrationForm.bind(this)}
                  >
                    Create
                  </button>
                </Col>
              </Row>
            </Form>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
export default CreateMyProfile;
