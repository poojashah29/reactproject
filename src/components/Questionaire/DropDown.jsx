import React, { Component } from "react";

class DropDownQuestions extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  createDropDownQuestions = () => {
    let table = [];

    // Outer loop to create parent
    for (let i = 0; i < this.props.fields; i++) {
      let children = [];
      //Inner loop to create children
      for (let j = 0; j < this.props.options; j++) {
        children.push(<option>{"Artificial Intelligence"}</option>);
      }
      //Create the parent and add the children
      table.push(
        <div>
          <div>
            <span style={{ color: "#b7b5b5" }}>{this.props.title}</span>
            <span
              class="glyphicon glyphicon-info-sign"
              style={{ marginLeft: "10px" }}
            />
          </div>

          <select
            class="form-control inputBackground"
            style={{ marginBottom: "20px" }}
            id={"sel" + i}
          >
            {children}
          </select>
        </div>
      );
    }

    return table;
  };

  render() {
    return <div>{this.createDropDownQuestions()}</div>;
  }
}

export default DropDownQuestions;
