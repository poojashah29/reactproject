import React, { Component } from "react";
import Footer from "../Footer/Footer";
import { Progress } from "antd";
import Inner_Navbar from "../Navbar/Inner_Navbar";
import DropDownQuestions from "./DropDown";
import DetailedQuestions from "./Detailed";

class Questions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalQuestions: 13,
      solvedQuestions: 5,
      type: 1,
      Questions: {
        Q1: {
          type: 1,
          fields: 1,
          options: 4,
          title: "Select from available industries",
          question: "Which of the following best describes your industry?"
        },
        Q2: {
          type: 2,
          question: "Which of the following best describes your industry?"
        },
        Q3: {
          type: 3,
          question: "Which of the following best describes your industry?"
        },
        Q4: {
          type: 4,
          question: "Which of the following best describes your industry?"
        }
      }
    };
    this.Save = this.Save.bind(this);
    this.nextState = this.nextState.bind(this);
    this.previousState = this.previousState.bind(this);
  }
  nextState() {
    if (this.state.solvedQuestions < 13) {
      this.setState({
        solvedQuestions: this.state.solvedQuestions + 1
      });
    }

    if (this.state.type === 1) {
      this.setState({
        type: 0
        // Questions: {
        //   Q1: {
        //     question: "Why we hire you and why"
        //   }
        // }
      });
    } else {
      this.setState({
        type: 1
        // Questions: {
        //   Q1: {
        //     question: "Which of the following best describes your industry?"
        //   }
        // }
      });
    }
  }
  previousState() {
    if (this.state.solvedQuestions > 0)
      this.setState({
        solvedQuestions: this.state.solvedQuestions - 1
      });

    if (this.state.type === 1) {
      this.setState({
        type: 0
      });
    } else {
      this.setState({
        type: 1
      });
    }
  }
  Save() {
    window.location.hash = "/mycompanyprofile";
  }

  render() {
    return (
      <div>
        <div className="innerBackground bg_imgpadding">
          <Inner_Navbar />
        </div>
        <div className="modal-dialog QuestionModel">
          <div className="modal-content">
            <div className="modal-body" />
            <form style={{ minHeight: "400px" }}>
              <div style={{ backgroundColor: "#d7e8f7d4" }}>
                <div className="row topBar">
                  <div className="col-md-2 col-sm-2 col-xs-3">
                    <h4>
                      {this.state.solvedQuestions +
                        "/" +
                        this.state.totalQuestions}
                    </h4>
                  </div>
                  <div className="col-md-7 col-sm-7 col-xs-7">
                    <Progress
                      percent={
                        100 *
                        (this.state.solvedQuestions / this.state.totalQuestions)
                      }
                    />
                  </div>
                  <div className="col-md-3 col-sm-3 col-xs-2" id="saveBtn">
                    <a onClick={this.Save}>Save</a>
                  </div>
                </div>
                <div className="row">
                  <div className="questionheading">
                    <h4>Question</h4>
                  </div>
                </div>
                <div className="row" style={{ marginRight: "5px" }}>
                  <div className="question">
                    <span>
                      <strong>{this.state.Questions.Q1.question}</strong>
                    </span>
                  </div>
                </div>
              </div>
              <div>
                {this.state.type === 1 ? (
                  <div id="dropdownQuestion">
                    {
                      <DropDownQuestions
                        title={this.state.Questions.Q1.title}
                        options={this.state.Questions.Q1.options}
                        fields={this.state.Questions.Q1.fields}
                      />
                    }
                  </div>
                ) : (
                  <div id="detailedQuestion">{<DetailedQuestions />}</div>
                )}
                {/* Drop down Questions */}
                {/* <div id="dropdownQuestion">
                  {
                    <DropDownQuestions
                      title={this.state.Questions.Q1.title}
                      options={this.state.Questions.Q1.options}
                      fields={this.state.Questions.Q1.fields}
                    />
                  }
                </div> */}

                {/* Detailed Questions */}
                {/* <div id="detailedQuestion">{<DetailedQuestions />}</div> */}
                {/* personal Questions */}
                {/* <div id="personalQuestion">personal</div> */}
                {/* multiple choice Questions */}
                {/* <div id="multichoice"> multiple choice </div> */}
              </div>

              <div className="row">
                <button
                  type="submit"
                  className="btns_question"
                  onClick={this.previousState}
                >
                  Back
                </button>

                <button
                  type="submit"
                  className="btns_question"
                  onClick={this.nextState}
                >
                  Next
                </button>
              </div>
            </form>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Questions;
