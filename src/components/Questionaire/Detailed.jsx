import React, { Component } from "react";

class DetailedQuestions extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div class="form-group">
        <textarea
          class="form-control inputBackground"
          rows="7"
          id="detailedText"
        />
      </div>
    );
  }
}

export default DetailedQuestions;
