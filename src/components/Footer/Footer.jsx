import React, { Component } from "react";
import facebook from "../../icons/facebook.png";
import google from "../../icons/google.png";
import linkdin from "../../icons/linkdin.png";
import pinterest from "../../icons/pinterest.png";
import twitter from "../../icons/twitter.png";
import "../../style/App.css";
import { Link } from "react-router";
import icon from "../../images/icon.png";

class Footer extends Component {
  render() {
    return (
      <div id="Footer">
        <footer className="page-footer font-small cyan darken-3">
          <div className="container-fluid footerStyle">
            <div className="row" id="footer_Row">
              <div className="col-md-3 mx-auto">
                <h5 className="font-weight-bold mt-3 mb-4 footText footerTitle">
                  Platform
                </h5>
                <ul className="list-unstyled">
                  <li>
                    <a className="footText" href="#!">
                      Scheduling
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Content Curation
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Analytics
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Monitoring
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Team Managment
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Apps and Integrations
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-md-2 mx-auto">
                <h5 className="font-weight-bold mt-3 mb-4 footText footerTitle-M">
                  Plans
                </h5>
                <ul className="list-unstyled">
                  <li>
                    <a className="footText" href="#!">
                      Basic
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Professional
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Team
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Business
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Enterprise
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Compare Plans
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-md-2 mx-auto">
                <h5 className="font-weight-bold  mt-3 mb-4 footText footerTitle-M">
                  Partners
                </h5>
                <ul className="list-unstyled">
                  <li>
                    <a className="footText" href="#!">
                      Alliance
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Technology
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Digital Services
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Developer
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-md-3 mx-auto">
                <h5 className="font-weight-bold mt-3 mb-4 footText footerTitle">
                  About Us
                </h5>
                <ul className="list-unstyled">
                  <li>
                    <a className="footText" href="#!">
                      Contact Us
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Careers
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Newsroom
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Customers
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Enterprise
                    </a>
                  </li>
                  <li>
                    <a className="footText" href="#!">
                      Events
                    </a>
                  </li>
                </ul>
              </div>
              <div
                className="col-md-2 mx-auto"
                style={{ paddingLeft: "0px", paddingRight: "0px" }}
              >
                <div>
                  <Link to="/">
                    <img src={icon} alt="" />
                  </Link>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-md-12 py-5 paddingfooter">
                <a href="https://www.facebook.com/" target="_blank">
                  <img className="iconstyle_1" src={facebook} alt="" />
                </a>
                <a href="https://twitter.com/" target="_blank">
                  <img className="iconstyle" src={twitter} alt="" />
                </a>
                <a href="https://www.google.com/" target="_blank">
                  <img className="iconstyle" src={google} alt="" />
                </a>
                <a href="https://www.pinterest.com/" target="_blank">
                  <img className="iconstyle" src={pinterest} alt="" />
                </a>
                <a href="https://www.linkedin.com/" target="_blank">
                  <img className="iconstyle" src={linkdin} alt="" />
                </a>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

export default Footer;
